// On attend que le DOM soit chargé
$(function() {
  // Sélection d'un élément
  const $example = $("#example");

  // Utilisation d'une méthode en "setter"
  $example.text("<div>Hello JQuery :)</div>");

  // Utilisation d'une méthode en "getter"
  const text = $example.text();
  console.log(text);

  // On peut enchainer les appels, car la plupart des méthodes
  // retournent l'élément jQuery sur lequel elles sont appliquées
  $example
    .text("<div>Hello JQuery :)</div>")
    .css("backgroundColor", "#eee")
    .css("color", "blue")
    .css({
      fontWeight: "bold",
      fontStyle: "italic"
    });

  // Selection de plusieurs élements
  $(".amount")
    // Appliquer une fonction sur chacun des éléments
    .each(function() {
      // On stocke l'élément jQuery dans une variable pour des raisons de performances
      // (Appliquer plusieurs fois la fonction jQuery sur le même élément est couteux et inutile)
      const $this = $(this);
      const amount = parseFloat($this.text());

      if (amount > 20) {
        $this.css("backgroundColor", "red");
      }
    });

  // On peut "filter" une liste d'éléments pour ne garder
  // que ceux quinous intéressent

  // Filtrer avec un sélecteur
  const $bigButtons = $(".btn").filter(".btn-block");

  // Filtrer avec une fonction (si elle retourne true, l'élément "reste" dans la liste, sinon il est filtré)
  $list = $(".amount").filter(function() {
    return parseFloat($(this).text()) > 20;
  });

  console.log($list);

  // Cloner un élément
  $clonedTable = $("table").clone();
  // Et l'insérer dans le DOM
  $("body").append($clonedTable);
  // Ou
  $($clonedTable).appendTo("body");

  // Avec jQuery, on peut "enchainer" les appels,
  // ce qui améliore grandement la lisibilité du code

  $example
    .before($clonedTable)
    .css("background", "red")
    .wrap('<div class="container"><div class="inner-wrapper">');
});
