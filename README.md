# infrep-thunes

Une appli pour gérer les comptes entre "amis".

## Contraintes pour l'interface graphique

- Pouvoir ajouter des participants

  - Pseudo
  - Date d'arrivée
  - Date de départ

- Pouvoir ajouter des dépenses (associées à un participant)

  - Montant
  - Intitulé (court)
  - Date
  - Description (optionnelle)

- Afficher la liste des dépenses

- Pouvoir faire le calcul de répartition
